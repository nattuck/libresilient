const makeServiceWorkerEnv = require('service-worker-mock');

global.fetch = require('node-fetch');
jest.mock('node-fetch')

global.fetch.mockImplementation((url, init) => {
          const response = new Response(
                            new Blob(
                              [JSON.stringify({ test: "success" })],
                              {type: "application/json"}
                            ),
                            {
                              status: 200,
                              statusText: "OK",
                              headers: {
                                'ETag': 'TestingETagHeader'
                              },
                              url: url
                            });
          return Promise.resolve(response);
        });

describe("plugin: fetch", () => {
  beforeEach(() => {
    Object.assign(global, makeServiceWorkerEnv());
    jest.resetModules();
    global.LibResilientPluginConstructors = new Map()
    LR = {
        log: (component, ...items)=>{
            console.debug(component + ' :: ', ...items)
        }
    }
  })
  test("it should register in LibResilientPluginConstructors", () => {
    require("../../plugins/fetch.js");
    expect(LibResilientPluginConstructors.get('fetch')().name).toEqual('fetch');
  });
  
  test("it should return data from fetch()", async () => {
    require("../../plugins/fetch.js");

    const response = await LibResilientPluginConstructors.get('fetch')(LR).fetch('https://resilient.is/test.json');
    
    expect(fetch).toHaveBeenCalled();
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
  });
  
  test("it should set the LibResilient headers", async () => {
    require("../../plugins/fetch.js");

    const response = await LibResilientPluginConstructors.get('fetch')(LR).fetch('https://resilient.is/test.json');
    
    expect(fetch).toHaveBeenCalled();
    expect(await response.json()).toEqual({test: "success"})
    expect(response.url).toEqual('https://resilient.is/test.json')
    expect(response.headers.has('X-LibResilient-Method')).toEqual(true)
    expect(response.headers.get('X-LibResilient-Method')).toEqual('fetch')
    expect(response.headers.has('X-LibResilient-Etag')).toEqual(true)
    expect(response.headers.get('X-LibResilient-ETag')).toEqual('TestingETagHeader')
  });
  
  test("it should throw an error when HTTP status is >= 400", async () => {
    
    global.fetch.mockImplementation((url, init) => {
            const response = new Response(
                                new Blob(
                                  ["Not Found"],
                                  {type: "text/plain"}
                                ),
                                {
                                  status: 404,
                                  statusText: "Not Found",
                                  url: url
                                });
            return Promise.resolve(response);
          });
    
    require("../../plugins/fetch.js");

    expect.assertions(1)
    expect(LibResilientPluginConstructors.get('fetch')(LR).fetch('https://resilient.is/test.json')).rejects.toThrow(Error)
  });
});
